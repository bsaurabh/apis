public class Point2D
{
    private final double x, y;
    private final Comparator<Point2D> POLAR_ORDER = new PolarOrder();
    
    private static ccw(Point2D a, Point2D b, Point2D c)
    {
        // return based on determinant (i.e. signed area)
    }
    
    private class PolarOrder implements Comparator<Point2D>
    {
        public int compare(Point2D q1, Point2D q2)
        {
            double dx1 = q1.x - x;
            double dy1 = q1.y - y;
            
            double dx2 = q2.x - x;
            double dy2 = q2.y - y;
            if (dy1 == 0 && dy2 == 0)
            {
                System.out.println("Horizontal");
            }
            else if (dy1 >= 0 && dy2 < 0)
            {
                System.out.println("q1 above p and q2 below p");
            }
            else if (dy2 >= 0 && dy1 < 0)
            {
                System.out.println("q2 above p and q1 below p");
            }
            else
                return -ccw(Point2D.this, q1, q2);
                
        }
        
    }
}