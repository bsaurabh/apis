public class UnorderedMaxPQ <Key extends Comparable<Key>>
{
       private Key[] pq;
       private int N;   // number of elements in PQ
       
       public UnorderedMaxPQ(int capacity)
       {
           pq = (Key[])new Comparable[capacity];    // there is no generic array creation in Java
       }
       
       public boolean isEmpty()
       {
           return N == 0;
       }
       
       public void insert(Key x)
       {
           pq[N++] = x;
       }
       
       public Key delMax()
       {
           int max = 0;
           for (int i = 1; i < N; i++)
           {
               if (less(max, i))
                max = i;
           }
           exch(max, N-1);
           return pq[--N];
       }
}