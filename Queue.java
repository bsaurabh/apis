import java.util.Iterator;
public class Queue<Item> implements Iterable<Item>
{
	private Item[] data;
	private int first, last, length;
	
	public Queue()
	{
		// Default constructor
		data = (Item[])new Object[1];
		length = 1;
		first = -1;
		last = -1;
	}

    public Iterator<Item> iterator()
	{
	    return new ListIterator();
	}
	
	private class ListIterator implements Iterator<Item>
	{
	    private int current = first;
	    public boolean hasNext()
	    {
	        return (current < last);
	    }
	    public Item next()
	    {
	        Item element = data[current];
	        current += 1;
	        return element;
	    }
	    public void remove()
	    {
	        System.out.println("Iterator remove() is not supported");
	    }
	}

	private void rearrange()
	{
		// Shifts the queue elements back to begin from index 0
		for (int i = 0; i < (last - first + 1); i++)
		{
			data[i] = data[first + i];
			last--;
		}
		first = 0;
		
	}

	private void resize(int capacity)
	{
		Item[] tmp = data;
		data = (Item[]) new Object[capacity];
		for (int i = 0; i < length; i++)
			data[i] = tmp[i];
		length = capacity;
	}

	public void enqueue(Item element)
	{
		if ((last == length-1) && (first != 0))
			rearrange();
		
		else if ((last == length - 1))
			resize (2*length);

		data[++last] = element;
	}

	public Item dequeue()
	{
		Item element = data[first];
		first += 1;
		return element;
	}	

}
