public class QuickSort
{
    private static int partition(Comparable[] a, int lo, int hi)
    {
        while (true)
        {
            int i = lo, j = hi + 1;
        
            while (a[++i] < a[lo])
                if (i == hi)
                    break;
            while (a[--j] > a[lo])
                if (j == lo)
                    break;
        
            // check if entire array has been scanned
            if (i >= j)
                break;
            
            // swap a[i] and a[j]
            exch(a, i, j);
        }
        // swap a[lo] and a[j]
        exch(a, lo, j);
        return j;
    }
    
    public static void sort(Comparable[] a)
    {
        // shuffle the array
        StdRandom.shuffle(a);
        sort(a, 0, a.length - 1);
    }
    
    public static void sort(Comparable[] a, int lo, int hi)
    {
        int CUTOFF = 10;
        if (hi <= lo + CUTOFF - 1)
            Insertion.sort(a, lo, hi);
            return;
        // partition
        int j = partition(a, lo, hi);   // j is in correct position
        // can be sped up with median of 3 partitioning
        
        // recursive sort
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }
    
    // Selection algorithm
    public static int select (Comparable[] a, int k)
    {
        // find the kth smallest term (kth location in sorted array)
        // do a random shuffle
        StdRandom.shuffle(a);
        int lo = 0, hi = a.length - 1;
        while (true)
        {
            int j = partition (a, lo, hi);
            if (j > k)
                hi = j - 1;
            else if (j < k)
                lo = j + 1;
            else
                return a[k];
        }
        return a[k];
    }
    
}