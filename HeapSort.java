public class HeapSort <Key extends Comparable<Key>>
{
    private Key[] pq;
    private int N = 1;

    private void buildHeap()
    {
        // build heap from bottom up
        for (int k = N/2; k > 0; k--)
            sink(k);
    }
    
    private void sink(int k)
    {
        while(2*k <= N)
        {
            int j = 2*k;
            if(j+1 <= N && less(j, j+1))    j++;
            exch(j, k);
            k = j;
        }
    }
    
    private void exch(int a, int b)
    {
        Key temp = pq[a-1]; // -1 because array starts at 0 and heap at 1
        pq[a-1] = pq[b-1];
        pq[b-1] = temp;
    }
    
    private boolean less(int a, int b)
    {
        return pq[a-1].compareTo(pq[b-1]);
    }
    
    private void getLargestInHeap()
    {
        exch(1, N--);
        sink(1);
    }
    
    public static void sort(Comparable[] a)
    {
        this.pq = a;
        this.N = pq.length;
        buildHeap();
        
        while(N > 1)
            getLargestInHeap();
    }
    
}