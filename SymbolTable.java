public class SymbolTable
{
    public boolean contains(Key key)
    {
        return get(key) != null;
    }
    
    private void lazydelete(Key key)
    {
        put(key, null);
    }
}