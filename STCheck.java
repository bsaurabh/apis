public class STCheck
{
    public static void main(String[] args)
    {
        SymbolTable<String, Integer> st = new SymbolTable<String, Integer>();
        for (int i = 0; !StdIn.isEmpty(); i++)
        {
            st.put(StdIn.readString(), i);
        }
        
        // print
        for (String s : st.keys())
            System.out.println(s + ":" + st.get(s));
    }
}