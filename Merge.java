public class Merge
{
    public static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi)
    {
        assert isSorted(a, lo, mid);
        assert isSorted(a, mid+1, hi);
        
        for (int k = lo; k <= hi; k++)
        {
            aux[k] = a[k];  // copy the arrays
        }
        
        int i = lo, j = mid + 1;
        for (int k = lo; k <= hi; k++)
        {
            if (i > mid)
            {
                a[k] = aux[j++];
            }
            else if (j > hi)
            {
                a[k] = aux[i++];
            }
            if (less(aux[j], aux[i]))
            {
                a[k] = aux[j];
                j++;
                k++;
            }
            else
            {
                a[k] = aux[i];
                i++;
                k++;
            }
        }
        assert isSorted(a, lo, hi);    
    }
    
    public void sort (Comparable[] a, Comparable[] aux, int lo, int hi)
    {
        int CUTOFF = 7;
        if (hi <= lo + CUTOFF - 1)   return;
        int mid = (lo + hi) / 2;
        sort (a, aux, lo, mid);
        sort (a, aux, mid+1, hi);
        merge(a, aux, lo, mid, hi);
    }
    
    public void sort (Comparable[] a)
    {
        aux = new Comparable[a.length];
        sort(a, aux, 0, a.length-1);
    }
}