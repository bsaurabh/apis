public class bst <Key extends Comparable<Key>, Value>
{
    // binary search tree
    private Node root;
    
    private class Node
    {
        private Key key;
        private Value value;
        private Node left, right;
        
        public Node (key, val)
        {
            this.key = key;
            this.val = val;
        }
    }
    
    public Value get (Key key)
    {
        Node x = root;
        while (x != null)
        {
            int cmp = key.compareTo(x.key);
            if (cmp < 0)
                x = x.left;
            else if (cmp > 0)
                x = x.right;
            else
                return x.val;
        }
        return null;
    }
    
    public void put (Key key, Value val)
    {
        root = put (root, key, val);
    }
    
    private Node put(Node x, Key key, Value val)
    {
        if (x == null)
            return new Node(key, val);
        int cmp = key.compareTo(x.key);
        if (cmp < 0)
            x.left = put(x.left, key, val);
        else if (cmp > 0)
            x.right = put(x.right, key, val);
        else
            x.val = val;
        return x;
    }
    
    public Key floor (Key key)
    {
        Node x = floor(root, key);
        if (x == null)
            return null;
        return x.key;    
    }
    
    private Node floor (Node x, Key key)
    {
        if (x == null)
            return null;
        int cmp = key.compareTo(x.key);
        if (cmp < 0)
            return floor(x.left, key);
        Node temp = floor(x.right, key);
        if (temp == null)
            return x;
        else
            return temp;
    }
}