public class MaxPQ <Key extends Comparable <Key>>
{
    // Maximum returning Priority Queue implemented with binary heap
    
    private Key[] pq;
    private int N = 1;
    
    public MaxPQ(int capacity)
    {
        pq = (Key[])new Comparable[capacity+1]; // as we are not using index 0
    }
    
    public boolean isEmpty()
    {
        return N == 0;
    }
    
    private void swim(int k)
    {
        // swim up to position
        while ((k > 1) && less(int(k/2), k))
        {
            exch (k, int(k/2));
            k = int(k/2);
        }
    }
    
    public void insert(Key element)
    {
        pq[++N] = element;
        swim(N);
    }
    
    private void sink(int k)
    {
        // sink to appropriate position
        while (2*k < N)
        {
            int j = 2*k;
            // resolve power struggle between children
            if (j+1 < N && less(j, j+1))  j++;
            // demote k
            exch(k, j);
            k = j;
        }
    }
    
    public Key delMax()
    {
        Key max = pq[1];
        exch(1, N--);
        sink(1);
        pq[N+1] = null;
        return max;
    }
    
    private boolean less(int a, int b)
    {
        return pq[a].compareTo(pq[b]) < 0;  // Key extends Comparable
    }
    
    private void exch(int a, int b)
    {
        Key temp = pq[a];
        pq[a] = pq[b];
        pq[b] = temp;
    }
}