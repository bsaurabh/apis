import java.util.Iterator;
public class Stack<Item> implements Iterable<Item>
{
	// Array implementation of stack with array resizing
	private Item[] numbers;
	private int first;	// points to top most element in stack
	private int length;
	
	
	public Stack()
	{
		// Constructor
		numbers = (Item[])new Object[1];	// to avoid generic array error
		first = 0;
		length = 1;
	}
	public Iterator<Item> iterator()
	{
	    return new ListIterator();
	}
	
	private class ListIterator implements Iterator<Item>
	{
	    private int current = first;
	    public boolean hasNext()
	    {
	        return (current > 0);
	    }
	    public Item next()
	    {
	        Item element = numbers[current];
	        current -= 1;
	        return element;
	    }
	    public void remove()
	    {
	        System.out.println("Iterator remove() is not supported");
	    }
	}
	public int getLength()
	{
		// Accessor method for length
		return (length);
	}
	
	private void resize(int capacity)
	{
		// resizes the array to capacity
		Item[]  temp = numbers;
		numbers = (Item[])new Object[capacity];
		// copy from temp to numbers
		for (int i = 0; i < length; i++)
			numbers[i] = temp[i];
		
		length = capacity;
	}
	
	public void push(Item element)
	{
		// resize the stack array to twice its size
		if (first == length)
			resize(2 * length);
		
		// now push element on stack
		numbers[first++] = element;			
	}

	public Item pop()            
	{
		// pop an element from the stack
		Item element = numbers[--first];

		// reduce length of array when 1/4th occupied
		if (first <= length/4)
			resize (length/4);
		else
		    length--;
		
		return element;
	}

}
