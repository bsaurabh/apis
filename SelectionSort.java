public class SelectionSort
{
    public static void sort(Comparable[] a)
    {
        int N = a.length;
        
        for (int i = 0; i < N-1; i++)
        {
            int min = i;
            for (int j = i+1; j < N; j++)
            {
                if less(a[j],  a[min])  // a[j] < a[min]
                    min = j;
            }
            
            // swap minimum and i
            exch(a, i, min);
        }
    }
    
    private static boolean less(Comparable v, Comparable w)
    {
        // same as before
    }
    
    private static void exch(Comparable[] a, int i, int j)
    {
        // same as before
    }
    
}