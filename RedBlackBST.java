public class RedBlackBST <Key extends Comparable<Key>, Value>
{
    private boolean RED = true;
    private boolean BLACK = false;
    private class Node
    {
        private Key key;
        private Value val;
        private boolean color;
        private Node left, right;
        
        public Node(Key key, Value val, boolean color)
        {
            this.key = key;
            this.val = val;
            this.color = color;
        }
    }
    
    private Node put(Node h, Key key, Value val)
    {
        if (h == null)
            return new Node(key, val, RED);
        int cmp = key.compareTo(h.key); // extends Comparable
        if (cmp < 0)
            h.left = put(h.left, key, val);
        else if (cmp > 0)
            h.right = put(h.right, key, val);
        else
            h.val = val;
            
        // handle rotations to make left-leaning Red black BST
        // dont use else if as cases can happen consecutively
        if (isRed(h.right) && !isRed(h.left))   // 3-node leaning incorrrectly
            h = rotateLeft(h);
        if ((isRed(h.left) && isRed(h.left.left)))
            h = rotateRight(h);
        if (isRed(h.left) && isRed(h.right))
            flipColors(h);
    }
}